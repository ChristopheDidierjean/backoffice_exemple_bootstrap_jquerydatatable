<?php

    /// <summary>
    ///     database connection
    /// </summary>
    require_once('Connection/connection.php');

    /// <summary>
    ///     retrieve values from $_POST global variable
    /// </summary>
    $idContexte = $_POST['idContexte'];
    $idOrigine = $_POST['idOrigine'];
    $displayImageId = $_POST['displayImageId'];
    $displayImage = $_POST['displayImage'];
    
    try {

        /// <summary>
        ///     update image id on correspondance table to null 
        /// </summary>
        $updateCorrespondance = "   UPDATE correspondance
                                        INNER JOIN expressions
                                                ON correspondance.idOrigine = expressions.idExpression
                                        INNER JOIN se_rapporte
                                                ON expressions.idExpression = se_rapporte.idExpression
                                    SET idImage = ?
                                    WHERE idOrigine = ? AND idContexte = ?;";
        $connexion->getBdd()->prepare($updateCorrespondance)->execute([NULL, $idOrigine, $idContexte]);
        UNSET($updateCorrespondance);
        
        /// <summary>
        ///     delete image file on server
        /// </summary>
        $deleteImage = "    DELETE FROM images 
                            WHERE idImage = ?";
        $connexion->getBdd()->prepare($deleteImage)->execute([$displayImageId]);
        
        /// <summary>
        ///     delete image html values on database 
        /// </summary>
        UNLINK('../'.$displayImage);

        /// <summary>
        ///     set send-back-message when image file deleted from server and path deleted from database
        ///     and css style
        /// </summary>
        $returnArray =   [
            'error' => FALSE,
            'message' => 'Image deleted',
            'style' => 'text_green bold'
        ];

    } catch (Expception $e) {
        
        /// <summary>
        ///     set send-back-message when delete failed
        ///     and css style
        /// </summary>
        $returnArray =   [
            'error' => TRUE,
            'message' => 'Delete image failed',
            'style' => 'text_red bold',
            'exception' => $e
        ];
    }

    /// <summary>
    ///     write json on page
    /// </summary>
    echo json_encode($returnArray);
?>