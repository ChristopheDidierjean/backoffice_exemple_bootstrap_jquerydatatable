<?php

    /// <summary>
    ///     database connection
    /// </summary>
    require_once('Connection/connection.php');
    
    /// <summary>
    ///     deserialize in array with expression and id expression
    /// </summary>
    $dataArray = json_decode($_POST['data_json'], True);
    
    /// <summary>
    ///     prepare sql query
    /// </summary>
    $reqUpdate = "  UPDATE expressions 
                    SET expression = ? 
                    WHERE idExpression = ?;";

    /// <summary>
    ///     update database with each expression in array
    ///     define array with adequate response
    /// </summary>
    foreach($dataArray as $key){
        $idExpression = $key['idExpression'];
        $expression = $key['expression'];

        try{
            $connexion->getBdd()->prepare($reqUpdate)->execute([$expression, $idExpression]);

            /// <summary>
            ///     set send-back-message when datas updated
            ///     and css style
            /// </summary>
            $returnArray =   [
                'error' => FALSE,
                'message' => 'Datas updated',
                'style' => 'text_green bold'
            ];
        }catch(Exception $e){
            
            /// <summary>
            ///     set send-back-message when update failed
            ///     and css style
            /// </summary>
            $returnArray =   [
                'error' => TRUE,
                'message' => 'Update failed',
                'style' => 'text_red bold',
                'exception' => $e
            ];
        }
    }

    /// <summary>
    ///     write API's result on page
    /// </summary>
    /// <parameter>
    ///     $returnArray : array with values defined in API's process
    /// </parameter>
    echo json_encode($returnArray);
?>