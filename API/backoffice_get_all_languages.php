<?php

    /// <summary>
    ///     database connection
    /// </summary>
    require_once('Connection/connection.php');

    /// <summary>
    ///     sql query
    /// </summary>
    $reqGetAllLangages = "  SELECT idLangue, langue
                            FROM langues
                            WHERE langue NOT LIKE 'Français'";

    /// <summary>
    ///     execute sql request through connection object
    /// </summary>
    $resGetAllLangages = $connexion->getBdd()->query($reqGetAllLangages);

    /// <summary>
    ///     fetch database return and stock it in 2 dimensions array
    ///     surrounded by try catch if database return non compatible with fetch return
    /// </summary>
    try {
        $returnArray = $resGetAllLangages->fetchAll(PDO::FETCH_ASSOC); 
    } catch(Exception $e) {
        throw $e;
    }

    /// <summary>
    ///     brows array and makes its contents compatible with a display
    /// </summary>
    foreach($returnArray as $key1 => &$value1){
        foreach($value1 as $key2 => &$value2) {
            $value2 = str_replace("`", "'", $value2);
        }
    }

    /// <summary>
    ///     write API's result on page
    /// </summary>
    /// <parameter>
    ///     $returnArray : array with database query result
    /// </parameter>
    echo json_encode($returnArray);
?>