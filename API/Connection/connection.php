<?php

/// <summary>
///     call required files to instantiate connection object
/// </summary>
require_once('settings.php');
require_once('class.php');

/// <summary>
///     database connection instantiation
/// </summary>
/// <parameter>
///     $datasConnectOenolexDev : connection values
/// </parameter>
/// <return>
///     connection object
/// </return>
try {

    $connexion = new DBConnection($datasConnect);

    $retour["success"] = true;
} catch(Exception $e) {
    $retour["success"] = false;
}

?>
