<?php

/// <summary>
///     database connection class
/// </summary>
/// <parameter>
///     $datasConnection :  database connection values
///                         stored in settings.php
/// </parameter>
class DBConnection {
    private $datasConnection = [];
    private $bdd = '';

    public function __construct($datasConnection){
        $this->datasConnect = $datasConnection;
        $this->bdd = new PDO('mysql:host='.$this->datasConnect['dbHost'].';dbname='.$this->datasConnect['dbName'].'', $this->datasConnect['dbUser'], $this->datasConnect['dbPasswrd']);
        $this->bdd->EXEC ('SET CHARACTER SET UTF8');
        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    public function getDatasConnect(){
        return $this->datasConnect;
    }
    public function getBdd(){
        return $this->bdd;
    }
    
    public function setBdd($pBdd){
        return $this->bdd = $pBdd;
    }
    public function setDatasConnect($pDatasConnect){
        return $this->datasConnect = $pDatasConnect;
    }
    
}

?>