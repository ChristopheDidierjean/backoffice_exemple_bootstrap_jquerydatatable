<?php

    /// <summary>
    ///     database connection
    /// </summary>
    require_once('Connection/connection.php');

    /// <summary>
    ///     retrieve values from $_FILES global variable
    /// </summary>
    $image_name = $_FILES['imageFile']['name'];
    $image_path = _imageDir.$image_name;
    $tmp_name = $_FILES['imageFile']['tmp_name'];
    $image_size = $_FILES['imageFile']['size'];
    $error = $_FILES['imageFile']['error'];

    /// <summary>
    ///     retrieve values from $_POST global variable
    /// </summary>
    $idContexte = $_POST['idContexte'];
    $idOrigine = $_POST['idOrigine'];
    $idTraduction = $_POST['idTraduction'];
    $titre = $_POST['titre'];
    $altImage = $_POST['altImage'];
    $displayImageId = $_POST['displayImageId'];
    $displayImagePath = $_POST['displayImage'];

    /// <summary>
    ///     set send-back-message if error on file
    ///     write json on page
    ///     stop script
    /// </summary>
    if($error!==0) {
        $returnArray =   [
            'error' => TRUE,
            'message' => 'Unknown error occured on server',
            'style' => 'text_red bold'
        ];
        echo json_encode($returnArray);
        exit;    
    }

    /// <summary>
    ///     set send-back-message if file's too large
    ///     write json on page
    ///     stop script
    /// </summary>
    if($image_size>10000000) {
        $returnArray =   [
            'error' => TRUE,
            'message' => 'File too large',
            'style' => 'text_red bold'
        ];
        echo json_encode($returnArray);
        exit;    
    }
    
    /// <summary>
    ///     set send-back-message if already existing file with same name
    ///     write json on page
    ///     stop script
    /// </summary>
    if(file_exists($image_path)) {
        $returnArray =   [
            'error' => TRUE,
            'message' => 'Image name already used, choose another one ',
            'style' => 'text_red bold'
        ];
        echo json_encode($returnArray);
        exit;    
    }

    try{
        
        /// <summary>
        ///     delete image file and database reference if replaced
        ///     depending on expression and contexte
        /// </summary>
        if($displayImageId) {
            
            /// <summary>
            ///     update image id on correspondance table to null 
            /// </summary>
            $updateCorrespondance = "   UPDATE correspondance
                                            INNER JOIN expressions
                                                    ON correspondance.idOrigine = expressions.idExpression
                                            INNER JOIN se_rapporte
                                                    ON expressions.idExpression = se_rapporte.idExpression
                                        SET idImage = ?
                                        WHERE idOrigine = ? AND idContexte = ?;";
            $connexion->getBdd()->prepare($updateCorrespondance)->execute([NULL, $idOrigine, $idContexte]);
            UNSET($updateCorrespondance);
            
            /// <summary>
            ///     delete image file on server
            /// </summary>
            $deleteImage = "    DELETE FROM images 
                                WHERE idImage = ?";
            $connexion->getBdd()->prepare($deleteImage)->execute([$displayImageId]);
            
            /// <summary>
            ///     delete image html values on database 
            /// </summary>
            UNLINK('../'.$displayImagePath);
        }

        /// <summary>
        ///     insert in database values used in html code to display image
        /// </summary>
        /// <parameter>
        ///     $image_name : image name used as src link in img html tag
        ///     $titre : title used in img html tag
        ///     $altImage : alt used in img html tag 
        /// </parameter>
        /// <return>
        ///     $idImage : inserted value id in database
        ///</return>
        $insertImage = "    INSERT INTO images (lienImage, titre, altImage) 
                            VALUES (?, ?, ?);";
        $connexion->getBdd()->prepare($insertImage)->execute([$image_path, $titre, $altImage]);
        $idImage = $connexion->getBdd()->LastInsertId();

        /// <summary>
        ///     update correspondance table to link image to expression
        /// </summary>
        /// <parameter>
        ///     $idImage : image id to link
        ///     $idOrigine : id of the origine expression to bind to image
        ///     $idContexte : id of the contexte of the expression to bind
        /// </parameter>
        /// <return>
        ///     $idImage : inserted value id in database
        ///</return>
        $updateCorrespondance = "   UPDATE correspondance
                                        INNER JOIN expressions
                                                ON correspondance.idOrigine = expressions.idExpression
                                        INNER JOIN se_rapporte
                                                ON expressions.idExpression = se_rapporte.idExpression
                                    SET idImage = ?
                                    WHERE idOrigine = ? AND idContexte = ?;";
        $connexion->getBdd()->prepare($updateCorrespondance)->execute([$idImage, $idOrigine, $idContexte]);

        /// <summary>
        ///     copies the file to upload to the target folder on the server
        /// </summary>
        move_uploaded_file($tmp_name, '../'.$image_path);
        
        /// <summary>
        ///     set send-back-message when image downloaded and path put in database
        /// </summary>
        $returnArray =   [
            'error' => FALSE,
            'message' => 'Image downloaded',
            'style' => 'text_green bold',
            'image_name' => $image_path,
            'idImage' => $idImage
        ];

    }catch (Exception $e){

        /// <summary>
        ///     set send-back-message when adding image failed
        ///     and css style
        /// </summary>
        $returnArray =   [
            'error' => TRUE,
            'message' => 'Add image failed',
            'style' => 'text_red bold',
            'exception' => $e
        ];
    }

    /// <summary>
    ///     write json on page
    /// </summary>
    echo json_encode($returnArray);
?>