<?php

    /// <summary>
    ///     database connection
    /// </summary>
    require_once('Connection/connection.php');

    /// <summary>
    ///     sql query
    /// </summary>
    $reqGetAllExpressions = "        SELECT expressionsT.idLangue, langue, contextes.idContexte, contexte, expressionsO.expression ExpressionOrigine, 
                                            idOrigine, idTraduction,expressionsT.expression ExpressionTraduction, correspondance.idImage , lienImage
                                    FROM expressions expressionsO
                                            INNER JOIN correspondance
                                                    ON expressionsO.idExpression = correspondance.idOrigine
                                            INNER JOIN expressions expressionsT
                                                    ON correspondance.idTraduction = expressionsT.idExpression
                                            INNER JOIN se_rapporte se_rapporteO
                                                    ON expressionsO.idExpression = se_rapporteO.idExpression
                                            INNER JOIN se_rapporte se_rapporteT
                                                    ON expressionsT.idExpression = se_rapporteT.idExpression
                                            INNER JOIN contextes
                                                    ON se_rapporteO.idContexte = contextes.idContexte
                                            INNER JOIN langues
                                                    ON expressionsT.idLangue = langues.idLangue
                                            LEFT JOIN images
                                                    ON correspondance.idImage = images.idImage
                                    WHERE se_rapporteO.idContexte = se_rapporteT.idContexte
                                    ORDER BY idContexte, idOrigine;";
 
    /// <summary>
    ///     execute sql request through connection object
    /// </summary>
    $resGetAllExpressions = $connexion->getBdd()->query($reqGetAllExpressions);
    
    /// <summary>
    ///     fetch database return and stock it in 2 dimensions array
    ///     surrounded by try catch if database return non compatible with fetch return
    /// </summary>
    try {
        $returnArray = $resGetAllExpressions->fetchAll(PDO::FETCH_ASSOC); 
    } catch(Exception $e) {
        throw $e;
    }
    
    /// <summary>
    ///     brows array and makes its contents compatible with a display
    /// </summary>
    foreach($returnArray as $key1 => &$value1){
        foreach($value1 as $key2 => &$value2) {
            $value2 = str_replace("`", "'", $value2);
        }
    }
    
    /// <summary>
    ///     write API's result on page
    /// </summary>
    /// <parameter>
    ///     $returnArray : array with database query result
    /// </parameter>
    echo json_encode($returnArray);
?>