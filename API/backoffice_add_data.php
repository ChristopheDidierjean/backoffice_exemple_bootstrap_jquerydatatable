<?php

    /// <summary>
    ///     database connection
    /// </summary>
    require_once('Connection/connection.php');
    
    /// <summary>
    ///     deserialize in array with expression and id expression
    /// </summary>
    $dataArray = json_decode($_POST['data_json'], True);

    /// <summary>
    ///     variables initialistaion
    /// </summary>
    $idContexte = $dataArray['idContexte'];
    $expressions = $dataArray['expressions'];
    $correspondanceArray = [];
    $idImage = null;

    /// <summary>
    ///     expressions table prepared query initialistaion
    /// </summary>
    $getExpression = "  SELECT idExpression
                        FROM expressions
                        WHERE expression LIKE ?
                        AND idLangue = ?";

    $putExpression = "  INSERT INTO expressions (expression, idLangue)
                        VALUES (?, ?);";

    /// <summary>
    ///     se-rapporte table prepared query initialistaion
    /// </summary>
    $getSe_rapporte = " SELECT idExpression, idContexte
                        FROM se_rapporte
                        WHERE idExpression = ?
                        AND idContexte = ?";
    $putSe_rapporte = " INSERT INTO se_rapporte (idExpression, idContexte)
                        VALUES(?, ?);";

    /// <summary>
    ///     images table prepared query initialistaion
    /// </summary>
    $getImageId = "     SELECT idImage 
                        FROM correspondance
                            INNER JOIN expressions expressionsO
                                    ON correspondance.idOrigine = expressionsO.idExpression
                                INNER JOIN se_rapporte se_rapporteO
                                        ON expressionsO.idExpression = se_rapporteO.idExpression
                            INNER JOIN expressions expressionsT
                                    ON correspondance.idTraduction = expressionsT.idExpression
                                INNER JOIN se_rapporte se_rapporteT
                                        ON expressionsT.idExpression = se_rapporteT.idExpression
                        WHERE idOrigine = ?
                        AND se_rapporteO.idContexte = se_rapporteT.idContexte
                        AND se_rapporteO.idContexte = ?
                        GROUP BY idImage;";
    
    /// <summary>
    ///     correspondance table prepared query initialistaion
    /// </summary>
    $putCorrespondance = "  INSERT INTO correspondance (idOrigine, idTraduction, idImage)
                            VALUES (? ,? ,?);";

    /// <summary>
    ///     correspondance table prepared query initialistaion
    /// </summary>
    /// <parameter>
    ///     $expressions : array langue id and expression asoociated
    ///     $idLangue : langue id of the expression
    ///     $expression : expression
    /// </parameter>
    /// <return>
    ///     array with origin expression id on index 0
    ///     and traduction expression id on index 1
    /// </return>
    foreach($expressions as $key){

        /// <summary>
        ///     parameter initialisation from array key
        /// </summary>
        $expression = $key['expression'];
        $idLangue = $key['idLangue'];

        try{
            
            /// <summary>
            ///     get expression from database
            /// </summary>
            $statement = $connexion->getBdd()->prepare($getExpression);
            $statement->execute([$expression, $idLangue]);
            $getExpressionResult = $statement->fetchall(PDO::FETCH_ASSOC);
            
            /// <summary>
            ///     insert expression in database if non existing
            ///     else get expression id from previous query
            /// </summary>
            /// <return>
            ///     $idExpression : expression id
            /// </retunr>
            if(sizeof($getExpressionResult)==0) {
                $connexion->getBdd()->prepare($putExpression)->execute([$expression, $idLangue]);
                $idExpression = $connexion->getBdd()->lastInsertId();
            }else{
                $idExpression = $getExpressionResult[0]['idExpression'];
            }

        }catch(Exception $e){
           
            /// <summary>
            ///     set send-back-message when get/put expression from database failed
            ///     and css style
            ///     stop script
            /// </summary>
            $returnArray =   [
                'error' => TRUE,
                'message' => 'An error occured on query expression on database',
                'style' => 'text_red bold',
                'exception' => $e
            ];
            echo json_encode($returnArray);
            exit();
        }

        try{

            /// <summary>
            ///     check if expression exist in database in this contexte
            /// </summary>
            $statement = $connexion->getBdd()->prepare($getSe_rapporte);
            $statement->execute([$idExpression, $idContexte]);
            $getSe_rapporteResult = $statement->fetchall(PDO::FETCH_ASSOC);
            
            /// <summary>
            ///     bind expression and contexte in database
            /// </summary>
            if(sizeof($getSe_rapporteResult)==0) {
                $statement = $connexion->getBdd()->prepare($putSe_rapporte);
                $statement->execute([$idExpression, $idContexte]);
            }    
        }catch(Exception $e){
            
            /// <summary>
            ///     set send-back-message when get/put se_rapporte value failed
            ///     and css style
            ///     stop script
            /// </summary>
            $returnArray =   [
                'error' => TRUE,
                'message' => 'An error occured on query se_rapporte on database',
                'style' => 'text_red bold',
                'exception' => $e
            ];
            echo json_encode($returnArray);
            exit();
        }

        /// <summary>
        ///     push expression id in array
        ///     origine expression on index 0
        ///     traduction expression on index 1
        /// </summary>
        array_push($correspondanceArray, $idExpression);
    }

    /// <summary>
    ///     variables initialisation from correspondance array
    /// </summary>
    $idOrigine = $correspondanceArray[0];
    $idTraduction = $correspondanceArray[1];

    try{

        /// <summary>
        ///     get image id from database, even null value
        /// </summary>
        $statement = $connexion->getBdd()->prepare($getImageId);
        $statement->execute([$idOrigine, $idContexte]);
        $getImageIdResult = $statement->fetchall(PDO::FETCH_ASSOC);

        /// <summary>
        ///     get image id from database, even null value
        ///     put value on $idImage if database return value
        /// </summary>
        if(sizeof($getImageIdResult)==1) {
            $idImage = $getImageIdResult[0]['idImage'];
        }else if(sizeof($getImageIdResult)>1){
            
            /// <summary>
            ///     set send-back-message when database return several image id
            ///     and css style
            ///     stop script
            /// </summary>
            $returnArray =   [
                'error' => TRUE,
                'message' => 'More than one id image is returned by database',
                'style' => 'text_red bold',
            ];

            echo json_encode($returnArray);
            exit();
        }
    }catch (Excpetion $e){
            
        /// <summary>
        ///     set send-back-message when retrieve image id failed
        ///     and css style
        ///     stop script
        /// </summary>
        $returnArray =   [
            'error' => TRUE,
            'message' => 'An error occured on query select image id on database',
            'style' => 'text_red bold',
            'exception' => $e
        ];
        echo json_encode($returnArray);
        exit();
    }

    $statement = $connexion->getBdd()->prepare($putCorrespondance);
    $statement->execute([$idOrigine, $idTraduction, $idImage]);

    /// <summary>
    ///     set send-back-message when datas updated
    ///     and css style
    /// </summary>
    $returnArray =   [
        'error' => FALSE,
        'message' => 'Add datas success',
        'style' => 'text_green bold'
    ];

    /// <summary>
    ///     write API's result on page
    /// </summary>
    /// <parameter>
    ///     $returnArray : array with values defined in API's process
    /// </parameter>
    echo json_encode($returnArray);
?>