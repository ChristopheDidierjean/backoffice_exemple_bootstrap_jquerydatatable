/// <summary>
///     define as constant jquery getElementById
/// </summary>
const _update_title = $('#update_title');

/// <summary>
///     get values from fields
///     append them on ajax data
///     display message recieved from API
/// </summary>
/// <return>
///     updated view
/// </return>
$('.btn_update').click(function (){
    
    /// <summary>
    ///     get values from fields
    /// </summary>
    let idOrigine = $('#idOrigine_modal_update').val();
    let expressionOrigine = $('#expressionOrigine_modal_update').val();
    let idTraduction = $('#idTraduction_modal_update').val();
    let expressionTraduction = $('#expressionTraduction_modal_update').val();

    /// <summary>
    ///     prevent script to update expression with empty value
    /// </summary>
    if(expressionOrigine==''||expressionTraduction=='') {
        _update_title.text('At least one field is empty');
        _update_title.addClass('text_red bold');
        return;
    }

    /// <summary>
    ///     create array with values
    /// </summary>
    let dataArray = [
        {'idExpression' : idOrigine, 'expression' : expressionOrigine},
        {'idExpression': idTraduction, 'expression'  : expressionTraduction}
    ]

    /// <summary>
    ///     serialize array
    ///     adapt it for writing into database
    /// </summary>
    var json_string = JSON.stringify(dataArray);
    json_string = json_string.replace("'","`");
    
    /// <summary>
    ///     ajax method to APi for updating data
    /// </summary>
    $.ajax({
        url:_url_api_update_expression,
        type:'POST',
        datatype:'text',
        data:{
            'data_json':json_string,
        },
        success: function(api_return){
            
            /// <summary>
            ///     deserialize api's data return
            /// </summary>
            let data = JSON.parse(api_return);
            
            /// <summary>
            ///     display message on modal header
            /// </summary>
            _update_title.removeClass('text_green text_red bold')
            _update_title.text(data.message);
            _update_title.addClass(data.style);
            
            /// <summary>
            ///     refresh datatable json source
            ///     redraw datatable
            /// </summary>
            $('.display').DataTable().ajax.reload().draw();
        },
        
        /// <summary>
        ///     display message if ajax failed
        /// </summary>
        error: function(e){
            _update_title.text('Datas not send');
            _update_title.addClass('text_red bold');
            console.log(e.message);
        }
    })
});

/// <summary>
///     set modal to initial state
///     hide modal
/// </summary>
/// <return>
///     modal to initial state
/// </return>
function closeModalUpdate() {
    _update_title.text('Update expression');
    _update_title.removeClass('text_green text_red bold');
    $('.modal').modal('hide');
}