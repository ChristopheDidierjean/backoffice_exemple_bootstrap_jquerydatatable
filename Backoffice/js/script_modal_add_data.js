/// <summary>
///     define as constant several jquery getElementById
/// </summary>
const _add_data_title = $('#add_data_title');
const _select_context_tag = $('#select_context_tag');
const _select_language_tag = $('#select_language_tag');

$('document').ready(function(){
    
    /// <summary>
    ///     initialise storage array
    /// </summary>
    let addDataArray = {
        'idContexte':'',
        'expressions':'',
    };

    /// <summary>
    ///     ajax method to APi for fill select language options 
    /// </summary>
    $.ajax({
        url:_url_api_get_all_languages,
        success: function(api_return) {
            let data = JSON.parse(api_return);
            
            /// <summary>
            ///     add empty option in head of html option 
            /// </summary>
            let optionList='<option value=""></option>)';
            
            /// <summary>
            ///     concatenate html options filled with values returned by API
            /// </summary>
            for(let i=0; i<data.length;i++) {
                let idLangue = data[i]['idLangue'];
                let langue = data[i]['langue'];

                optionList += ('<option value="'+idLangue+'">'+langue+'</option>)');
            }
            
            /// <summary>
            ///     add html string on DOM
            /// </summary>
            _select_language_tag.html(optionList);
        }
    })

    /// <summary>
    ///     ajax method to APi for fill select context options 
    /// </summary>
    $.ajax({
        url:_url_api_get_all_context,
        success: function(api_return) {
            let data = JSON.parse(api_return);
            
            /// <summary>
            ///     add empty option in head of html option 
            /// </summary>
            let optionList='<option value=""></option>)';
            
            /// <summary>
            ///     concatenate html options filled with values returned by API
            /// </summary>
            for(let i=0; i<data.length;i++) {
                let idContexte = data[i]['idContexte'];
                let contexte = data[i]['contexte'];
                optionList += ('<option value="'+idContexte+'">'+contexte+'</option>)');
            }

            /// <summary>
            ///     add html string on DOM
            /// </summary>
            _select_context_tag.html(optionList);
        }
    })

    $('.btn_add_data').click(function (){
        
        /// <summary>
        ///     get values from fields
        /// </summary>
        let expressionOrigine = $('#expressionOrigine_modal_add_data').val();
        let expressionTraduction = $('#expressionTraduction_modal_add_data').val();
        let idLangue = _select_language_tag.val();
        let idContexte = _select_context_tag.val();
        
        /// <summary>
        ///     prevent script to add expressions with empty value
        /// </summary>
        if(expressionOrigine==''||expressionTraduction==''||idLangue==''||idContexte=='') {
            _add_data_title.text('At least one field is empty');
            _add_data_title.addClass('text_red bold');
            return;
        }

        /// <summary>
        ///     store values in array
        /// </summary>
        addDataArray.idContexte = idContexte;
        addDataArray.expressions = [
            {'idLangue': 1, 'expression':expressionOrigine},
            {'idLangue': idLangue, 'expression':expressionTraduction},
        ];
                
        /// <summary>
        ///     serialize array
        ///     adapt it for writing into database
        /// </summary>
        var json_string = JSON.stringify(addDataArray);
        json_string = json_string.replace("'","`");

        /// <summary>
        ///     ajax method to add image
        /// </summary>
        $.ajax({
            url:_url_api_add_data,
            type:'POST',
            datatype:'text',
            data:{
                'data_json':json_string,
            },
            success: function(api_return){
                
                /// <summary>
                ///     deserialize api's data return
                /// </summary>
                let data = JSON.parse(api_return);
                
                /// <summary>
                ///     display message on modal header
                /// </summary>
                _add_data_title.text(data.message);
                _add_data_title.addClass(data.style);        
            
                /// <summary>
                ///     refresh datatable json source
                ///     redraw datatable
                /// </summary>
                $('.display').DataTable().ajax.reload().draw();
            },
            
            /// <summary>
            ///     display message if ajax failed
            /// </summary>
            error: function(e){
                _update_title.text('Datas not send');
                _update_title.addClass('text_red bold');
                console.log(e.message);
            }
        })
    });
})

/// <summary>
///     set modal to initial state
///     hide modal
/// </summary>
/// <return>
///     modal to initial state
/// </return>
function closeModalAddData() {
    
    $('#expressionOrigine_modal_add_data').val('');
    $('#expressionTraduction_modal_add_data').val('');
    _select_language_tag.val('');
    _select_context_tag.val('');
    _add_data_title.text('Add image');
    _add_data_title.removeClass('text_green text_red bold');

    $('.modal').modal('hide');
}
