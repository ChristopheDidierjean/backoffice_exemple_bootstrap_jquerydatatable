/// <summary>
///     constants used in scripts
/// </summary>

/// <summary>
///     api's path
/// </summary>
const _url_api_get_all_expressions = '../API/backoffice_get_all_expressions.php';
const _url_api_get_all_context = '../API/backoffice_get_all_context.php';
const _url_api_get_all_languages = '../API/backoffice_get_all_languages.php';
const _url_api_update_expression = '../API/backoffice_update_expression.php';
const _url_api_add_image = '../API/backoffice_add_image.php';
const _url_api_delete_image = '../API/backoffice_delete_image.php';
const _url_api_add_data = '../API/backoffice_add_data.php';


/// <summary>
///     images path
///     default image name
/// </summary>
const _src_image_dir = '../images/';
const _default_image = 'default_no_photo.jpg';