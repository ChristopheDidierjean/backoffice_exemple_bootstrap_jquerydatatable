/// <summary>
///     define as constant jquery getElementById
/// </summary>
const _add_image_title = $('#add_image_title');

$('.btn_add_image').click(function (){
    
    /// <summary>
    ///     get values from fields
    /// </summary>
    let idOrigine = $('#idOrigine_modal_add_image').val();
    let idTraduction = $('#idTraduction_modal_add_image').val();
    let idContexte = $('#idContext_modal_add_image').val();
    let imageTitle = $('#image_title').val();
    let altImage = $('#alt_image').val();
    let displayImageId = $('#idImage_modal_add_image').val();
    let displayImage = $('#lienImage_modal_add_image').val();

    /// <summary>
    ///     get file to upload
    /// </summary>
    let imageFile = $('#image_file')[0].files;

    /// <summary>
    ///     prevent script to add image with empty files or fields
    /// </summary>
    if(imageFile.length=0||imageTitle==''||altImage=='') {
        _add_image_title.text('Select an image and/or complete fields');
        _add_image_title.addClass('text_red bold');
        return;
    }

    /// <summary>
    ///     check if file to upload is image type of
    ///     display message on modal header if not
    ///     clear selected file if not
    /// </summary>
    if(!imageFile[0].type.match('image/*')) {
        _add_image_title.text('Wrong type of file');
        _add_image_title.addClass('text_red bold');
        $('#image_file').val('');
        return;
    };

    /// <summary>
    ///     create a new form
    ///     append image file to upload
    ///     append values
    /// </summary>
    /// <return>
    ///     form with files and post variables
    /// </return>
    let formData = new FormData();
    formData.append('imageFile', imageFile[0]);
    formData.append('idContexte', idContexte);
    formData.append('idOrigine',idOrigine);
    formData.append('idTraduction', idTraduction);
    formData.append('titre', imageTitle);
    formData.append('altImage', altImage);
    formData.append('displayImageId', displayImageId);
    formData.append('displayImage', displayImage);

    /// <summary>
    ///     ajax method to add image
    /// </summary>
    $.ajax({
        url: _url_api_add_image,
        type:'post',
        data:formData,
        contentType:false,
        processData:false,
        success: function(api_return){     

            /// <summary>
            ///     deserialize api's data return
            /// </summary>
            let data = JSON.parse(api_return);
            
            /// <summary>
            ///     display message send by API on modal header
            /// </summary>
            _add_image_title.removeClass('text_green text_red bold')
            _add_image_title.text(data.message);
            _add_image_title.addClass(data.style);

            /// <summary>
            ///     switch displayed image by uploaded image
            /// </summary>
            $('#idImage_modal_add_image').val(data.idImage);
            $('#lienImage_modal_add_image').val(data.image_name);
            $('#image_modal_add_image').attr('src', '../'+data.image_name);
            $('#image_modal_add_image').fadeOut(1).fadeIn(1000);
            
            /// <summary>
            ///     refresh datatable json source
            ///     redraw datatable
            /// </summary>
            $('.display').DataTable().ajax.reload().draw();
        }
    })
});

/// <summary>
///     switch between modals
/// </summary>
$('.btn_delete_image').click(function (){
    $('.modal').modal('hide');
    $('#modal_delete_confirmation').modal('show');

});

/// <summary>
///     set modal to initial state
///     hide modal
/// </summary>
/// <return>
///     modal to initial state
/// </return>
function CloseModalAddImage() {
    _add_image_title.text('Add image');
    _add_image_title.removeClass('text_green text_red bold');
    $('#image_file').val('');
    $('#image_title').val('');
    $('#alt_image').val('');
    $('#idImage_modal_add_image').val('');
    $('#image_modal_add_image').attr('src', _src_image_dir+_default_image);
    $('.modal').modal('hide');
}

/// <summary>
///     delete image path and association on database
///     delete image file on server
///     hide modal
/// </summary>
/// <parameter>
///     confirmationValue : yes or no
/// </parameter>
/// <return>
///     diplay table updated
///     return to modal add image
/// </return>
function DeleteImage(confirmationValue) {
    
    /// <summary>
    ///     delete image file and image path if confirmed
    /// </summary>
    if(confirmationValue=='yes') {
        
        /// <summary>
        ///     get values from fields
        /// </summary>
        let displayImageId = $('#idImage_modal_add_image').val();
        let displayImage = $('#lienImage_modal_add_image').val();
        let idOrigine = $('#idOrigine_modal_add_image').val();
        let idContexte = $('#idContext_modal_add_image').val();
    
        /// <summary>
        ///     create a new form
        ///     append values
        /// </summary>
        /// <return>
        ///     form with post variables
        /// </return>
        let formData = new FormData();
        formData.append('displayImageId', displayImageId);
        formData.append('displayImage', displayImage);
        formData.append('idOrigine', idOrigine);
        formData.append('idContexte', idContexte);

        /// <summary>
        ///     ajax method to delete image
        /// </summary>    
        $.ajax({
            url: _url_api_delete_image,
            type:'post',
            data:formData,
            contentType:false,
            processData:false,
            success: function(api_return){
    
                /// <summary>
                ///     deserialize api's data return
                /// </summary>
                let data = JSON.parse(api_return);
                
                /// <summary>
                ///     switch displayed image by uploaded image
                /// </summary>
                $('#idImage_modal_add_image').val('');
                $('#image_modal_add_image').attr('src', _src_image_dir+_default_image);
                $('#image_modal_add_image').fadeOut(1).fadeIn(1000);
    
                /// <summary>
                ///     refresh datatable json source
                ///     redraw datatable
                /// </summary>
                $('.display').DataTable().ajax.reload().draw();
    
            }
        })
    }
    
    /// <summary>
    ///     switch between modals
    /// </summary>
    $('#modal_delete_confirmation').modal('hide');
    $('.modal').modal('show');
}