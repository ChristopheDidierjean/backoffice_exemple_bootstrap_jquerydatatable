$('.document').ready(function(){

    /// <summary>
    ///     initialise and draw jquery datatable on html table
    /// </summary>
    /// <parameter>
    ///     json file from API
    /// </parameter>
    /// <return>
    ///     html view
    /// </return>
    var table = $('.display').DataTable({
        
        /// <summary>
        ///     disabled ordering function in header and footer
        /// </summary>
        ordering: false,

        /// <summary>
        ///     specify json data source
        /// </summary>
        /// <parameter>
        ///     url: json data source url
        ///     dataSrc : modify data form to manipulate
        ///               initialise to empty to conserve sourced data form
        /// </parameter>
        ajax:{
            url:_url_api_get_all_expressions,
            dataSrc:''
        },

        /// <summary>
        ///     specify json data keys used in each html columns
        ///     add class on certains columns
        ///     render checkbox on Image column
        ///     add html as default content on certains columns in each row
        /// </summary>
        columns:[
            {
                data:'idLangue'
            },
            {
                data:'langue'
            },
            {
                data:'idContexte',
                className:'id_field text-center'
            },
            {data:'contexte'},
            {data:'ExpressionOrigine'},
            {
                data:'idOrigine',
                className:'id_field text-center'
            },
            {data:'idTraduction'},
            {data:'ExpressionTraduction'},
            {
                data:'idImage',
                className:'id_field text-center',
                render: function(data, type, row) {
                    if (data!='') {
                        return '<input type="checkbox" disabled checked>';
                    } else {
                        return '<input type="checkbox" disabled>';
                    }
                }
            },
            {data:'lienImage'},
            {
                defaultContent:'<button class="btn action-btn btn_modal_update"><i class="fa-regular fa-pen-to-square fa-lg"></i></button>  <button class="btn action-btn btn_modal_add_image"><i class="fa-solid fa-image fa-lg"></i></button>',              
            }
        ],

        /// <summary>
        ///     order data an column number (start to 0) ascendant
        /// </summary>
        'order':[[5, 'asc']],

        /// <summary>
        ///     hide numbered columns by default
        /// </summary>
        'columnDefs':[
            {
                'targets':[0,2,9],
                'visible':false
            }
        ],

        /// <summary>
        ///     number of row display on table
        /// </summary>
        'iDisplayLength':15,

        /// <summary>
        ///     dom configuration
        ///     necessary to modifiy datatable
        /// </summary>
        /// <parameter>
        ///     b : buttons extension
        ///     f : filtering input
        ///     r : processing display element
        ///     t : the table
        ///     i : table information summary
        ///     p : pagination control
        /// </parameter>
        'dom':'Bfrtip',

        /// <summary>
        ///     add buttons on datatable header
        /// </summary>
        buttons:[
            
            /// <summary>
            ///     hide/show columns button
            ///     native from jquery datatable
            /// </summary>
            'colvis',

            /// <summary>
            ///     personnalised button
            ///     add function on it
            ///     show modals targeted by jquery
            /// </summary>
            {
                text:'Add data',
                className:'btn btn-primary',
                action:function(e,dt,node,config){
                    $('#modal_add_data').modal('show');
                }
            }
        ],

        /// <summary>
        ///     function used once dom complete
        /// </summary>
        initComplete: function() {
            
            /// <summary>
            ///     add select filter on numbered column header
            ///     values from the column
            /// </summary>
            /// <return>
            ///     html select with values
            /// </return>
            this.api().column(3).every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.header()))
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        column.search(val ? '^' + val + '$' : '', true, false).draw();
                    });

                column.data().unique().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
            });
            this.api().column(1).every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.header()))
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        column.search(val ? '^' + val + '$' : '', true, false).draw();
                    });

                column.data().unique().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
            });

            /// <summary>
            ///     define src on default image 
            /// </summary>
            $('#image_modal_add_image').attr('src', _src_image_dir+_default_image);
        }
    });
        
    /// <summary>
    ///     actions on buttons defined in columns default content 
    /// </summary>

    /// <summary>
    ///     search values on actioned row
    ///     put them on specific fields in modal
    ///     show modal
    /// </summary>
    $('.display').on('click', 'button.btn_modal_update', function(){

        let data = table.row($(this).closest('tr')).data();

        $('#context_modal_update').val(data.contexte);
        $('#idOrigine_modal_update').val(data.idOrigine);
        $('#expressionOrigine_modal_update').val(data.ExpressionOrigine);
        $('#idTraduction_modal_update').val(data.idTraduction);
        $('#expressionTraduction_modal_update').val(data.ExpressionTraduction);
        
        $('#modal_update').modal('show');
    });

    /// <summary>
    ///     search values on actioned row
    ///     put them on specific fields in modal
    ///     show modal
    /// </summary>
    $('.display').on('click', 'button.btn_modal_add_image', function(){
        
        let data = table.row($(this).closest('tr')).data();

        $('#idContext_modal_add_image').val(data.idContexte);
        $('#idOrigine_modal_add_image').val(data.idOrigine);
        $('#idTraduction_modal_add_image').val(data.idTraduction);
        $('#idImage_modal_add_image').val(data.idImage);
        $('#lienImage_modal_add_image').val(data.lienImage);

        /// <summary>
        ///     change html img src value if existing
        /// </summary>
        if(data.idImage!=''){
            $('#image_modal_add_image').attr('src', '../'+data.lienImage);
        }

        $('#context_modal_add_image').val(data.contexte);
        $('#expressionOrigine_modal_add_image').val(data.ExpressionOrigine);
        $('#expressionTraduction_modal_add_image').val(data.ExpressionTraduction);

        $('#modal_add_image').modal('show');
    })
});