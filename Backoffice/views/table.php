<div class="container">
        <h4 class="text-center">Bonjour <?=$_SESSION['pseudo']?></h4>
        <div class="d-flex justify-content-center">
            <a class="btn btn-outline-secondary text-center" href="index.php?view=authentification&action=deco">Deconnexion</a>
        </div>

    <!-- <summary> -->
    <!--    html table header and footer -->
    <!-- </summary> -->
    <table class="display cell-border nowrap">
        <thead>
            <tr>
                <th>idLangue</th>
                <th></th>
                <th>idContexte</th>
                <th></th>
                <th>Français</th>
                <th>idFrançais</th>
                <th>idTraduction</th>
                <th>Traduction</th>
                <th>Image</th>
                <th>lienImage</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tfoot>
                <th>idLangue</th>
                <th>Langue</th>
                <th>idContexte</th>
                <th>Contexte</th>
                <th>Français</th>
                <th>idFrançais</th>
                <th>idTraduction</th>
                <th>Traduction</th>
                <th>Image</th>
                <th>lienImage</th>
                <th>Actions</th>
        </tfoot>
    </table>
</div>

<!-- <summary> -->
<!--    modals section -->
<!-- </summary> -->
<?php
    include('modals/modal_add_data.html');
    include('modals/modal_update_data.html');
    include('modals/modal_add_image.html');
?>