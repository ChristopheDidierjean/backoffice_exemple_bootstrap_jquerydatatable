<div class="authentification_view">
    <div class="authentification_window">
        <?php 
            
            /// <summary>
            ///     display message regarding user actions
            /// </summary>
            if($_SESSION['authentification']!='false'){
                echo "<h4>Veuillez vous identifier</h4>";
            }else{
                echo"<h4>Mot de passe invalide</h4>";
            }
        ?>
        
        <!-- <summary> -->
        <!--    authentification view -->
        <!-- </summary> -->
        <form method="POST" action="index.php?view=authentification">
            <label for="idNom" class="authentification_label">Pseudo : </label>
            <input type="text" name="idNom" required class="authentification_input">
            <br>
            <label for="idPsswrd" class="authentification_label">MdP : </label>
            <input type="password" name="idPsswrd" required class="authentification_input">
            <br><br>
            <input type="submit" name="subButn" value="Connexion">
        </form>    
    </div>
</div>