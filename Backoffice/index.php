<?php
    /// <summary>
    ///     open session
    ///     define authentification to none
    /// </summary>
    session_start();
    $_SESSION['authentification'] = 'none';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="css/styles.css"> 
    <link rel="stylesheet" href="libraries/Bootstrap-5.2.0-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="libraries/DataTables/datatables.css">
    <link rel="stylesheet" href="libraries/Fontawesome/css/fontawesome.css">
    <link rel="stylesheet" href="libraries/Fontawesome/css/brands.css">
    <link rel="stylesheet" href="libraries/Fontawesome/css/solid.css">

    <script src="libraries/JQuery/jquery-3.6.1.min.js"></script>
    <script src="libraries/Bootstrap-5.2.0-dist/js/bootstrap.min.js"></script>
    <script src="libraries/DataTables/datatables.min.js"></script>
    <script src="js/settings.js" defer></script>
    <script src="js/script_table_view.js" defer></script>
    <script src="js/script_modal_update.js" defer></script>
    <script src="js/script_modal_add_image.js" defer></script>
    <script src="js/script_modal_add_data.js" defer></script>

    <title>Backoffice Oenolex développement</title>
</head>
<body>
    <?php
    
        /// <summary>
        ///     manage deconnexion action
        /// </summary>
        /// <parameter>
        ///     $_GET['action] : value in $_GET global value
        /// </parameter>
        if(isset($_GET['action'])) {
            if($_GET['action']=='deco') {
                session_destroy();
                unset($_SESSION['id']);
            }
        }

        /// <summary>
        ///     check if existing user in database for authentification
        ///     fix view to display regarding to result
        /// </summary>
        /// <parameter>
        ///     $_POST['subBtn] : value in $_POST global variable
        /// </parameter>
        /// <return>
        ///     $_SESSION['pseudo'] : valid pseudo
        ///     $_GET['view'] : the view to display
        ///     $_SESSION['authentification'] : value of authentification
        /// </return>
        if(isset($_POST['subButn'])) {

            /// <summary>
            ///     initialise database connection
            /// </summary>
            require_once('../API/Connection/connection.php');
        
            /// <summary>
            ///     query database on user
            /// </summary>
            $req = "SELECT `pseudo` FROM `users` WHERE `pseudo` = '".$_POST['idNom']."' AND `mdp` = '".$_POST['idPsswrd']."'";
            $res = $connexion->getBdd()->query($req);
            $tbl = $res->fetchAll(PDO::FETCH_ASSOC);
            
            /// <summary>
            ///     defined the view based on the database result
            /// </summary>
            if(sizeof($tbl)>0) {
                $_SESSION['pseudo'] = $tbl[0]['pseudo'];
                $_GET['view'] = "table";
            } else {
                $_SESSION['authentification'] = 'false';
                $_GET['view'] = "authentification";
            }    
        }

        /// <summary>
        ///     display view
        /// </summary>
        /// <parameter>
        ///     $_GET['view'] : the view to display
        /// </parameter>
        /// <return>
        ///     view
        /// </return>
        if(!isset($_GET['view'])) $_GET['view'] = "authentification";
        $pageAffiche = "views/".$_GET['view'].".php";
        if (file_exists($pageAffiche)) {
            include($pageAffiche);
        } else {
            include('views/404.html');
        }
    ?>
</body>
</html>